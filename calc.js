let form = document.getElementById('xIsWhatPerecentOfY'); //this is the id for the form
let xinput = document.getElementById('xinput'); // this is the id for the input data 
let yinput = document.getElementById('yinput');
let resultfield = document.getElementById('resultfield'); // thisis the id for displaying the result

let mycal = function(event) { // I added the variable "event" so that I can call it later. By default, form submissions always clear the data in the input field but we need the data to stay on. I used the "preventDefault();" to prevent it from clearing. 

    if (!xinput.value || !yinput.value) { //check if the input fields are not empty 
        alert("Please eneter values in both fields");
    } else {
        let x = parseFloat(xinput.value); // we used the paersefloat function to convert the values into numbers. By default, javascript values are strings
        let y = parseFloat(yinput.value);

        let result = (x / y) * 100;

        resultfield.innerText = "The answer is " + result + "%"; //.innerText is used when there is no input field but you want to display content on the called id; in this case, "resultfield" is the name of the id
        event.preventDefault();
    }

}
form.addEventListener('submit', mycal);