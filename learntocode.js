// var name = 'Kayode';
// var age = 24;
// var message = 'My name is ' + name + ' ' + 'I am ' + age + ' years old';

// console.log(message);


// using the if condition to determin the greater number between 3 numbers
var acountbalance = 300;
var nikeairforce = 90.89;
var randomnum = 450

if (nikeairforce <= acountbalance) {
    acountbalance -= nikeairforce;
    console.log('we just bought a new shoe');

    console.log('new account balance is ' + acountbalance);

}

if (acountbalance > nikeairforce && acountbalance > randomnum) {
    console.log(acountbalance + ' is the larger number');
}

if (nikeairforce > acountbalance && nikeairforce > randomnum) {
    console.log(nikeairforce + ' is the larger number');
}
if (randomnum > acountbalance && randomnum > nikeairforce) {
    console.log(randomnum + ' is the larger number');
}
// learning how to re - arrange array items using the "copyWithin()" function
var fruits = ["Banana", "Orange", "Apple", "Mango", "Kiwi", "Papaya"];

console.log(fruits);

rearranged = fruits.copyWithin(2, 3, 5);

console.log(rearranged);


// learning how to usin the enteries() function
// var fruitlist = fruits.entries();
// console.log(fruitlist);

// learning how to use the push and pop command. By default pop() removes the last item on the array list while push will add to an array list 
var fruits = ["Banana", "Orange", "Apple", "Mango", "Kiwi", "Papaya"];

var newfruitlist = []; //created an empty array list to push into

console.log(newfruitlist.push(fruits[1])); //i pushed the array item in index 1

console.log('I pushed 1 into this new array list. Here is the new array list: ' + newfruitlist + '.'); //result is going to be Orange
console.log('the new length of the array is ' + newfruitlist.length);

// using the "let" instead of teh usual "var xyz = xxxx"
let testinglet = fruits.pop();

console.log(testinglet) //result is going to be papaya. Papaya was deleted from the fruits array. PS: The last items are deleted when .pop() is used




//learning how to use the array.splice() function. It can be used to remove and addnew item to an array. It usually return the item that was removed

let splicelist = fruits.splice(2, 1, "Coconut", "Damson", "Zucchini");

console.log('here is the removed item from the list:- ' + splicelist); //this is going to return the item that was removed from the array. In this case, it is Apple

console.log('here is the new list of fruits:- ' + fruits); //print out the new array including the new fruits


//----------------------------------------------------------------------------------------------------------------------------------------------------------------
//for loop

let indexnumber = 10;
for (x = 0; x < indexnumber; x++)
    console.log(x);


// for loop exercise - print out the names in the array 

let students = ['John', 'Snow', 'Cole', 'Seymone', 'Tunde']; //created an array

for (x = 0; x < students.length; x++) // you can just use x++
//loop through the array to print out the tems in the array
{
    console.log(students[x]); //print out to the console
}



//--------------------------------------------------------------------------------------------------------------------------------

//little excercise to calculate the area of three rectangles and put the results in an array.
function areaRec(length, width) { //created a function to calculate the area
    return length * width;
}

let arrayRec = []; //created an empty array to push the area into


arrayRec.push(areaRec(10, 5)); //pushed the result of the area into the empty array 
arrayRec.push(areaRec(10, 6));
arrayRec.push(areaRec(9, 5));

console.log(arrayRec); // print out the result

//----------------------------------------------------------------------------------------------------------------------------------------

//I want to practice how to use functions. 
//Storing a function in a varibale 


let areaTri = function(base, height) {
    return .5 * base * height;
}

console.log(areaTri(10, 6));
console.log(areaTri(12, 4));
console.log(areaTri(15, 6));

//----------------------------------------------------------------------------------------------------------------------------------------
//how to put a function in a variable and use in another function

let personDetails = [];

let personName = function name(first, last) {
    console.log('Firstname: ' + first, +' Last name: ' + last);
}

let personCard = function cardDetails(last4digit, cardCompany) {
    console.log('The last four digits of your card ' + last4digit + ' Card was given by ' + cardCompany);
}

personDetails.push(personName);
personDetails.push(personCard);

console.log(personDetails);
//----------------------------------------------------------------------------------------------------------------------------------------
//familiarizing with Objects 
let studentsAgain = []; //an empty array to push all the students into

//first way of creating objects
let student0 = {
    firstname: 'Jonathan',
    lastname: 'Kennedy',
    age: 15
};
//another way of creating objects 
let student1 = {};
student1.firstname = 'Ife';
student1.lastname = 'Grace';
student1.age = 19;

//another way of creating objects

let student2 = new Object();
student2.firstname = 'Ghost';
student2.lastname = 'Sean';
student2.age = 18;

//pushing the objects into the array
studentsAgain.push(student0);
studentsAgain.push(student1);
studentsAgain.push(student2);


//printing the result to the console

console.log(studentsAgain);
//------------------------------------------------------------------------------------------------------------
//still on objects. Now I can create a function inside the an object . 

// create a function that accept name of and age of students

let stdnts = []; //created an empty array to push students into.

function stdnt(first, last, age) { // creating a constructor function 
    this.fname = first; //using "this" to specify that you are referring to the fname in this function
    this.lname = last;
    this.age = age;
    this.greetings = function() { //creating a function inside an object
        return 'Hi, My name is ' + this.fname + ' ' + this.lname + ', I am ' + this.age + ' years old.'; // the purpsoe of this function is just to return greetings when called
    }
}

stdnts.push(new stdnt('Kay', 'Bone', 5)); //pushing this students into the array 
stdnts.push(new stdnt('Jame', 'Bondy', 7));
stdnts.push(new stdnt('Nbaku', 'Bose', 6));

for (indexnum = 0; indexnum < stdnts.length; indexnum++) { //loop through the array to print out the new array
    console.log(stdnts[indexnum]);

    // trying to call the "greetings" fuction in the "stdnt" function. PS: "greetings" is a local function in stdnt
    var stdntmessage = stdnts[indexnum];
    console.log(stdntmessage.greetings()); //print out the greetings for each student each time the loop is done
}